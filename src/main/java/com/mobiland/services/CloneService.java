package com.mobiland.services;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.Executors;

@Service
public class CloneService {

    public void cloneRepository() throws GitAPIException {
        Git git = Git.cloneRepository()
                .setURI( "https://github.com/omiddagala/karafeed.git" )
                .setDirectory( new File("/Users/omid/Desktop/test/"))
                .call();
    }

    public void runCliCommand() throws IOException, InterruptedException {
        ProcessBuilder builder = new ProcessBuilder();
        builder.command("ionic", "cordova", "build", "android");
        builder.directory(new File("/Users/omid/Desktop/test/"));
        Process process = builder.start();
        StreamGobbler streamGobbler =
                new StreamGobbler(process.getInputStream(), System.out::println);
        Executors.newSingleThreadExecutor().submit(streamGobbler);
        int exitCode = process.waitFor();
        assert exitCode == 0;
    }

    public static void main(String[] args) throws GitAPIException, IOException, InterruptedException {
        new CloneService().runCliCommand();
    }
}
