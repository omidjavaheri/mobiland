package com.mobiland;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MobilandApplication {

	public static void main(String[] args) {
		SpringApplication.run(MobilandApplication.class, args);
	}

}
